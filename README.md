# The Imaging Center of the IGBMC


## [Overview](./overview/overview.md)


## [Technologies](./technologies/technologies.md)


## [List of Equipment](./equipment/equipment.md)


## [How to Acces the Facility](./access/access.md)


## [The Team](./team/team.md)


## [User Guides](./guides/guides.md)


## [Image Analysis](./image_analysis/image_analysis.md)


## [Acknowledging the Facility](./acknowledgements/acknowledgements.md)


## [Publications](./publications/publications.md)
