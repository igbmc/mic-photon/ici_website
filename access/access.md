## First time access to the facility
1. **_IGBMC staff_**
    1. first complete the [User Account Creation Form](https://ppms.eu/igbmc/areq/?pf=3)
    2. upon activation of your PPMS account, please fill the [training request form](https://ppms.eu/igbmc/req/?pf=3&training=true&form=3)

2. **_Non-IGBMC User_**
    1. Fill [this form](./access/formulaire-demande-autorisations-externe.pdf)
    2. Return it to the [facility](mailto:groupe-mic-photon@igbmc.fr)
    3. The facility will contact you as soon as possible to organise a first session with you

## Booking instrument
The facility's instruments are bookable via our [PPMS instance](https://ppms.eu/igbmc/?MicPhoton).

## Requesting Assistance
You can request assistance when booking your session in PPMS. Assistance will be confirmed by a member of staff. Last minute assistance request may not be accepted due to staff availability limitations.

## General Facility Rules
1. User must read and signed the [facility charter](https://ppms.eu/igbmc/vdoc/?pf=3&docid=13) to gain access to the facility.
2. Autonomous access to a specific instrument will be granted after the appropriate training by a member of staff.
3. Usage of the facility instruments for imaging, data transfer, preheating of the environmental chamber, etc. must be booked via [PPMS](https://ppms.eu/igbmc/?MicPhoton).
4. The sample preparation space, cell culture incubators, cell culture hoods must be booked via the facility staff.
5. Editing/Cancelation of a session is possible up to 24h prior to the start of the booked session. At shorter notice please ask a member of staff for assistance to edit your booking(s).
6. The booked session(s) duration must match the real usage of the instrument, especially for time-lapse experiment. The facility staff will adjust the real usage time accordingly.
7. The facility staff core hours are 9:00-18:00, Monday to Friday. In the absence of the facility staff the keys for the instruments rooms must be requested at the IGBMC's security desk.
8. Neither installation of software nor modifications of existing software/hardware settings are allowed on the facility computers.
9. Disassembling or modifications of any components of imaging systems even in the case of malfunctioning is forbidden.
10. Replacing objectives or filter cubes is forbidden. These interventions must be requested a day in advance at the latest and is subject to staff availability.
11. Changing the location of the desk lamps is forbidden.
12. Changing the temperature of the microscope rooms is forbidden.
13. Changing the temperature and gas parameters of the facility cell culture incubators is forbidden. 

## General Health and Safety Rules
1. Both eating and drinking are strictly forbidden in the instruments, cell culture and sample preparation rooms.
2. Deactivating laser interlocks is forbidden.
3. Going behind the systems is forbidden.
4. Wearing gloves while using microscopes is forbidden.
5. Personal Protection Equipment must be worn in the sample preparation and the cell culture rooms

## Data Management
1. Connecting external devices to the microscope's computer is strictly forbidden.
2. Your data must not be stored on the microscope's computer. At the end of your session, transfer your data to your team storage solution.
3. The facility staff may erase data without notice to make space for the next experiment.
4. The data should not be visualized or processed on the microscope's computer.

## Cleaning at the end of your session
1. The objectives, sample mounting stages, benches and desks must be left clean. Any leftovers will be discarded without notice.
2. If not instructed otherwise, systems must be switched off by the last user of the day.
3. Incidents must be reported immediately to the facility staff (via PPMS in absence of the facility staff). Incidents are but not only: microscopes and computers malfunctions, breaking of coverslips, leakages of immersion oil or media etc.

## Waiting time
All requests are dealt in a chronological order, no priority access is granted. Delays may happen depending of the staff availability. The team does meet up every monday morning to organise the training and assistance for the coming week. 
