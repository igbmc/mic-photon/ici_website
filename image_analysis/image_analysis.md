“The Journal of Cell Biology performed a detailed study over the past
decade ... The study found that 10% of articles accepted for publication
included inappropriate manipulations of image data that contravened
journal policy, even if they did not alter the conclusions drawn from the
data (see International Society of Managing and Technical Editors, 2013).
A surprisingly large number of the authors appeared unaware that they
had handled image data inappropriately and, in many cases, were not
conscious of the ethical issues and consequences of their actions.”
Martin, C., Blatt, M., 2013. Manipulation and misconduct in the
handling of image data. Plant Cell 25, 3147–8.