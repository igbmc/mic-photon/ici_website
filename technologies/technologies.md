The photonic microscopy facility of the ICI provides access to:
- brightfield and epifluorescent [widefield microscopy](https://gitlab.com/igbmc/mic-photon/ici_website/-/blob/main/equipment/equipment.md#widefield-microscopy) and [macroscopy](https://gitlab.com/igbmc/mic-photon/ici_website/-/blob/main/equipment/equipment.md#macroscopy), [videomicroscopy](https://gitlab.com/igbmc/mic-photon/ici_website/-/blob/main/equipment/equipment.md#videomicroscopy).
- [confocal](https://gitlab.com/igbmc/mic-photon/ici_website/-/blob/main/equipment/equipment.md#confocal-microscopy) (point-scanning, multiphoton, Nipkow spinning disk) microscopy
- [light-sheet microscopy](https://gitlab.com/igbmc/mic-photon/ici_website/-/blob/main/equipment/equipment.md#light-sheet-microscopy)
- [laser microdissection microscopy](https://gitlab.com/igbmc/mic-photon/ici_website/-/blob/main/equipment/equipment.md#laser-microdissection)
- [image processing and analysis expertises](https://gitlab.com/igbmc/mic-photon/ici_website/-/tree/main#image-analysis)
- [cell culture and sample preparation laboratory space](https://gitlab.com/igbmc/mic-photon/ici_website/-/tree/main#image-analysis)